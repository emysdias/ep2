import java.lang.Thread;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Toolkit;
import javax.swing.ImageIcon;
import javax.swing.JPanel;
import javax.swing.Timer;
import java.awt.event.KeyEvent;
import javax.swing.JTextField;
import java.awt.BorderLayout;
import javax.swing.SwingConstants;
import java.util.Random;

public class InterfaceComum extends JPanel implements ActionListener {

	private static final long serialVersionUID = 1L;
	private final int LARGURA = 550;
	private final int ALTURA = 400;
	private final int TAMANHO_PARTES = 10;
	private final int BLOCO = (ALTURA * LARGURA)/(TAMANHO_PARTES * TAMANHO_PARTES);
	private final int BURACOS = 10;
	private final int buracoX[] = new int[BURACOS];
	private final int buracoY[] = new int[BURACOS];
	private final int x[] = new int[BLOCO];
	private final int y[] = new int [BLOCO];

	private int comprimento,dx,dy, score;
	private String direcao;
	private Image face, comida, big, obstaculo, corpo,bomba;
	private Image decrease;
	private Image escolha;
	private boolean comeu = false;
	private boolean jogando = true;

	private Timer timer;
	private JTextField Score;
	

	public InterfaceComum() {

		addKeyListener(new SnakeKeyListener());
		setPreferredSize(new Dimension(550, 400));
		setFocusable(true);
		Score = new JTextField();
		Score.setPreferredSize(new Dimension(5, 15));
		Score.setHorizontalAlignment(SwingConstants.CENTER);
		setBackground(Color.decode("#B8FF7B"));
		Score.setText("Score: " + score);
		add(Score, BorderLayout.NORTH);
		Score.setColumns(7);

		new Thread() {

			public void run() {
				Imagens();
				IniciarJogo ();
			}
		}.start();
	}

	private void Imagens() {
		ImageIcon rosto = new ImageIcon("src/imagens/rosto.png");
		face = rosto.getImage();
		ImageIcon body = new ImageIcon("src/imagens/corpo.png");
		corpo = body.getImage();
		ImageIcon maca = new ImageIcon("src/imagens/maca.png");
		comida = maca.getImage();
		ImageIcon maior = new ImageIcon("src/imagens/big.png");
		big = maior.getImage();
		ImageIcon bomb = new ImageIcon("src/imagens/bomb.png");
		bomba = bomb.getImage();
		ImageIcon decreaseFruit = new ImageIcon("src/imagens/decrease.png");
		decrease = decreaseFruit.getImage();
		ImageIcon buraco = new ImageIcon("src/imagens/buraco.png");
		obstaculo = buraco.getImage();

		timer = new Timer(100, this);
		timer.start();
	}

	private void LocalizarObstaculos() {
		Random random = new Random();

		for (int i = 0; i < BURACOS; i++) {
			buracoX[i] = TAMANHO_PARTES * random.nextInt(50);
			buracoY[i] = TAMANHO_PARTES * random.nextInt(50);
		}
	}

	private void LocalizarPonto() {
		Random escolhe_tipo = new Random();
		boolean conflito = false;
		int x, y, q;
		
		q = escolhe_tipo.nextInt(4);

		if(q == 0) {
			escolha = big;
		} else if(q == 1) {
			escolha = comida;
		} else if (q == 2) {
			escolha = bomba;
		} else if (q == 3) {
			escolha = decrease;
		}

		do {
			Random random = new Random();
			x = TAMANHO_PARTES * random.nextInt(30);
			y = TAMANHO_PARTES * random.nextInt(30);	

			for (int i = 0; i < BURACOS; i++) {
				if (x == buracoX[i] && y == buracoY[i]) {
					conflito = true;
					break;
				}
			}
		} while (conflito);
		dx = x;
		dy = y;
		conflito = false;			
	}

	private void Localizar() { 

		new Thread() {
			public void run() {
				while(true) {
					if(!jogando) {
						return;
					} else if (comeu) {
						comeu = false;
						return;
					} else {
						try {
							Thread.sleep(4000);
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
						LocalizarPonto();
					} 
				}
			}
		}.start();
	}

	private void IniciarJogo() {
		comprimento = 3;
		for (int z = 0; z < comprimento; z++) {
			x[z] = 50 - z * 10;
			y[z] = 50;
		}
		LocalizarObstaculos();
		LocalizarPonto();
		Localizar();
		return;
	}

	private void comendo() {
		if ((x[0] == dx) && (y[0] == dy)) {
			if(escolha == bomba) {
				jogando = false;
				return;
			} else if(escolha == decrease) {
				comprimento = 3;
			} else {
				comprimento++;
				new Thread() {
					public void run() {
						score++;
					}
				}.start();
				if(escolha == comida) {
					new Thread() {
						public void run() {
							score++;
						}
					}.start();
				}
			}

			new Thread() {
				public void run() {
					Score.setText("Score: " + score);
				}
			}.start();

			comeu = true;
			LocalizarPonto();
			Localizar();
			return;
		}
	}

	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);

		if(jogando) {
			for(int i = 0; i<BURACOS; i++) {
				g.drawImage(obstaculo, buracoX[i], buracoY[i], this);
			}
			g.drawImage(escolha, dx, dy, this);
			for (int j = 0; j < comprimento; j++) {
				if (j == 0) {
					g.drawImage(face, x[j], y[j], this);
				} else {
					g.drawImage(corpo, x[j], y[j], this);
				}
			}
			Toolkit.getDefaultToolkit().sync();  
		} else {
			finished(g);
			timer.stop();
		}	

	}

	private void move() {

		for (int i = comprimento ; i>0 ; i--) {
			y[i] = y[(i-1)];
			x[i] = x[(i-1)];
		}

		if (direcao=="right") {
			x[0] += TAMANHO_PARTES;
		}

		if (direcao=="left") {
			x[0] -= TAMANHO_PARTES;
		}

		if (direcao=="up") {
			y[0] -= TAMANHO_PARTES;
		}

		if (direcao=="down") {
			y[0] += TAMANHO_PARTES;
		}
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if(jogando) {
			verificaColisao();
			comendo();
			move();
		}
		repaint();
	}

	private void verificaColisao() {
		for(int i = comprimento ; i>0 ; i--) {
			if(i>4 && (x[0] == x[i]) && (y[0] == y[i])){
				jogando = false;
			}
		}

		for (int i = 0; i < BURACOS; i++) {
			if (x[0] == buracoX[i] && y[0] == buracoY[i]) {
				jogando = false;
			}
		}

		if(x[0] < 0){
			jogando = false;
		} else if(x[0] == LARGURA) {
			jogando = false;
		} else if(y[0] < 0) {
			jogando = false;
		} else if(y[0] == ALTURA) {
			jogando = false;
		}

	}

	private class SnakeKeyListener extends KeyAdapter {
		public void keyPressed(KeyEvent e) {

			int key = e.getKeyCode();

			if ((key == KeyEvent.VK_LEFT) && !(direcao == "right")) {
				direcao = "left";
			}
			else if ((key == KeyEvent.VK_RIGHT) && !(direcao == "left")) {
				direcao = "right";
			}
			else if ((key == KeyEvent.VK_UP) && !(direcao == "down")) {
				direcao = "up";
			}
			else if ((key == KeyEvent.VK_DOWN) && !(direcao == "up")) {
				direcao = "down";
			} 

		}
	};

	private void finished(Graphics g) {
		setBackground(Color.decode("#E96161"));
		
		Font fonte = new Font("Verdana", Font.BOLD, 60);		
		g.setColor(Color.black);
		g.setFont(fonte);
		g.drawString("Game over", 90, 200);
		return;
	}
}
