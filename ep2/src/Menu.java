import java.awt.EventQueue;
import javax.swing.JFrame;
import javax.swing.JPanel;
import java.awt.Dimension;
import java.awt.Color;
import javax.swing.JButton;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JLabel;
import javax.swing.ImageIcon;

public class Menu extends MenuPrincipal{

	private static final long serialVersionUID = 1L;
	private JPanel contentPane;

	public static void main(final String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Menu frame = new Menu();
					frame.setLocationRelativeTo(null);
					frame.setVisible(true);
		
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public Menu() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		dispose();

		setBounds(400, 100, 550, 400);
		setLocationRelativeTo(null);
		contentPane = new JPanel();
		contentPane.addMouseListener(new MouseAdapter() {

			public void mouseClicked(MouseEvent e) {
			}
		});

		setContentPane(contentPane);
		setTitle("Menu Inicial");
		contentPane.setLayout(null);

		JButton Jogar = new JButton("Jogar");
		JButton Instrucoess = new JButton("Instruções");
		JButton Sair = new JButton("Sair");


		Jogar.setBounds(20, 77, 200, 50);
		Jogar.setBackground(new Color(213, 230, 150));
		contentPane.add(Jogar);
		Instrucoess.setBounds(20, 157, 200, 50);
		Instrucoess.setBackground(new Color(213, 230, 150));
		contentPane.add(Instrucoess);
		Sair.setBounds(20, 237, 200, 50);
		Sair.setBackground(new Color(213, 230, 150));
		contentPane.add(Sair);

		JLabel Label = new JLabel("snake");
		Label.setIcon(new ImageIcon(Menu.class.getResource("/imagens/snake1.png")));
		Label.setMaximumSize(new Dimension(30, 30));
		Label.setBounds(0, -50, 550, 500);
		contentPane.add(Label);

		Jogar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				MenuCobras c = new MenuCobras();
				c.setVisible(true);
				dispose();
			}
		});

		Instrucoess.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				Instrucoes i = new Instrucoes();
				i.setVisible(true);
				dispose();
				
			}
		});

		Sair.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				System.exit(0);
			}
		});
	}

}


