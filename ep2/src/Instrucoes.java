import java.awt.Color;
import java.awt.EventQueue;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;


public class Instrucoes extends JFrame{


	private static final long serialVersionUID = 1L;
	private JPanel contentPane;

	public static void main(final String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Instrucoes frame = new Instrucoes();
					frame.setLocationRelativeTo(null);
					frame.setVisible(true);
		
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	public Instrucoes(){
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		dispose();
		setBounds(400, 100, 550, 400);
		setLocationRelativeTo(null);
		contentPane = new JPanel();
		contentPane.addMouseListener(new MouseAdapter() {

			public void mouseClicked(MouseEvent e) {
			}
		});
		
		setContentPane(contentPane);
		setTitle("Instruções");
		contentPane.setLayout(null);
		
		JButton voltar = new JButton("Voltar");
		
		voltar.setBounds(165, 337, 200, 50);
		voltar.setBackground(new Color(213, 230, 150));
		contentPane.add(voltar);
		
		JLabel Label = new JLabel("Instruções");
		Label.setIcon(new ImageIcon(Instrucoes.class.getResource("/imagens/instrucoes.png")));
		Label.setBounds(0, -50, 550, 500);
		contentPane.add(Label);
		
		voltar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				Menu m = new Menu();
				m.setVisible(true);
				dispose();
			}
		});
	}

}
