import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.Random;

import javax.swing.JPanel;


public class JogoSimples extends JPanel implements KeyListener, Runnable {

	private static final long serialVersionUID = 1L;
	public static final int WIDTH = 550;
	public static final int HEIGHT = 400;	
	private Graphics2D g2d;
	private BufferedImage image;
	private Thread thread;
	private boolean running;
	private long targetTime;
	private final int SIZE = 10;
	private Movimentacao doce,head;
	private ArrayList<Movimentacao>snake;
	private int score;
	private int level;
	private boolean gameOver;
	private int dx,dy,x,y;
	private boolean up,down,right,left,start;


	public JogoSimples(){

		setPreferredSize(new Dimension(WIDTH, HEIGHT));
		setFocusable(true);
		requestFocus();
		addKeyListener(this);
	}



	public void addNotify(){
		super.addNotify();
		thread = new Thread(this);
		thread.start();
	}

	private void setFPS(int fps){
		targetTime = 600/fps;
	}

	public void keyPressed(KeyEvent e) {
		int k = e.getKeyCode();
		if(k==KeyEvent.VK_UP) up = true;
		if(k==KeyEvent.VK_DOWN) down = true;
		if(k==KeyEvent.VK_RIGHT) right = true;
		if(k==KeyEvent.VK_LEFT) left = true;
		if(k==KeyEvent.VK_ENTER) start = true;
	}

	public void keyReleased(KeyEvent e) {
		int k = e.getKeyCode();
		if(k==KeyEvent.VK_UP) up = false;
		if(k==KeyEvent.VK_DOWN) down = false;
		if(k==KeyEvent.VK_RIGHT) right = false;
		if(k==KeyEvent.VK_LEFT) left = false;
		if(k==KeyEvent.VK_ENTER) start = false;
	}


	public void run() {
		if(running)return;
		init();

		long startTime;
		long elapsed;
		long wait;

		while(running){
			startTime = System.nanoTime();
			Random random = new Random();
			int numero = random.nextInt(4);
			update(numero);
			requestRender(numero);

			elapsed = System.nanoTime() - startTime;
			wait = targetTime - elapsed / 1000000;

			if(wait > 0){
				try{
					Thread.sleep(wait);

				}catch(Exception e){
					e.printStackTrace();
				}
			}
		}

	}


	private void setUpLevel(){

		snake = new ArrayList<Movimentacao>();
		head = new Movimentacao(SIZE);
		head.setPosition(WIDTH/2, HEIGHT/4);
		snake.add(head);

		for(int i=1 ; i<3 ; i++){
			Movimentacao e = new Movimentacao(SIZE);
			e.setPosition(head.getX() + (i*SIZE), head.getY());
			snake.add(e);
		}
		doce = new Movimentacao(SIZE);
		score = 0;
		gameOver = false;
		level=1;
		dx=dy=0;
		setFPS(level*10);
	}

	private void requestRender(int num) {
		render(g2d,num);
		Graphics g = getGraphics();
		g.drawImage(image, 0, 0, null);
		g.dispose();
	}

	private void update(int number) {
		if(gameOver){
			if(start){
					System.out.println("oi");
			}
			return;
		}
		if(up && dy == 0){
			dy = -SIZE;
			dx = 0;
		}
		if(down && dy == 0){
			dy = SIZE;
			dx = 0;
		}
		if(left && dx == 0){
			dy =0;
			dx = -SIZE;
		}
		if(right && dx == 0 && dy !=0){
			dy = 0;
			dx = SIZE;
		}

		if(dx!=0 || dy!=0){

			for(int i = snake.size()-1; i>0 ; i--){

				snake.get(i).setPosition(
						snake.get(i-1).getX(),
						snake.get(i-1).getY()				
						);
			}
			head.move(dx, dy);
		}

		for(Movimentacao e: snake){
			if(e.isCollision(head)){
				gameOver = true;
				break;
			}
		}
		if(doce.isCollision(head)){
			score++;
			int x = (int)(Math.random()*(WIDTH - 20 - SIZE));
			int y = (int)(Math.random()*(HEIGHT - 20 - SIZE));
			x = x-(x%SIZE-20);
			y = y-(y%SIZE-20);

			doce.setPosition(x, y);

			Movimentacao e = new Movimentacao(SIZE);
			e.setPosition(-100,-100);
			snake.add(e);

			if(score%10 == 0){
				level++;
				if(level > 10) level = 10;
				setFPS(level*10);
			}
		}



		if(head.getX() < 0) head.setX(WIDTH);
		if(head.getY() < 0) head.setY(HEIGHT);
		if(head.getX() > WIDTH) head.setX(0);
		if(head.getY() > HEIGHT) head.setY(0);
	}


	public void render(Graphics2D g2d, int num){

		g2d.clearRect(0, 0, WIDTH, HEIGHT);
		g2d.setColor(Color.black);
		g2d.setBackground(Color.decode("#99cc00"));

		for(Movimentacao e:snake){
			e.render(g2d);
		}

		g2d.setColor(Color.yellow);
		doce.render(g2d);


		if(gameOver){

			running=false;
			g2d.setBackground(Color.decode("#E96161"));
			g2d.clearRect(x, y, WIDTH, HEIGHT); //limpa tudo


			Font fonte = new Font("Verdana", Font.BOLD, 60);		
			g2d.setColor(Color.black);
			g2d.setFont(fonte);
			g2d.drawString("Game over", 90, 200);

			return;

		}

		g2d.setColor(Color.black);
		g2d.drawString("Score: " + score + " Level: " + level, 30, 30);

		if(dx==0 && dy==0){
			g2d.drawString("Ready!!", 150, 200);
		}

	}


	private void init() {

		image = new BufferedImage(WIDTH, HEIGHT, BufferedImage.TYPE_INT_ARGB);
		g2d = image.createGraphics();
		running = true;
		setUpLevel();	
	}



	@Override
	public void keyTyped(KeyEvent arg0) {


	}

}
