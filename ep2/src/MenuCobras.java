import javax.swing.JFrame;
import javax.swing.JPanel;
import java.awt.Dimension;
import java.awt.Color;
import javax.swing.JButton;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JLabel;
import javax.swing.ImageIcon;

public class MenuCobras extends MenuPrincipal{

	private static final long serialVersionUID = 1L;
	

	public MenuCobras() {
		JPanel contentPane;
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		dispose();
		
		setLocationRelativeTo(null);
		contentPane = new JPanel();
		contentPane.addMouseListener(new MouseAdapter() {
			
			public void mouseClicked(MouseEvent e) {
			}
		});
		
		setContentPane(contentPane);
		setTitle("Menu Inicial");
		contentPane.setLayout(null);
		
		JButton comum = new JButton("Cobra Comum");
		JButton kitty = new JButton("Cobra Kitty");
		JButton star= new JButton("Cobra Star");
		JButton simples= new JButton("Jogo Simples");
		
		
		comum.setBounds(20, 57, 200, 50);
		comum.setBackground(new Color(140, 250, 232));
		contentPane.add(comum);
		kitty.setBounds(20, 127, 200, 50);
		kitty.setBackground(new Color(250, 140, 140));
		contentPane.add(kitty);
		star.setBounds(20, 197, 200, 50);
		star.setBackground(new Color(255, 205, 0));
		contentPane.add(star);
		simples.setBounds(20, 267, 200, 50);
		simples.setBackground(new Color(137, 91, 230));
		contentPane.add(simples);
		
		JLabel Label = new JLabel("snake");
		Label.setIcon(new ImageIcon(Menu.class.getResource("/imagens/image.png")));
		Label.setMaximumSize(new Dimension(30, 30));
		Label.setBounds(0, -50, 550, 500);
		contentPane.add(Label);
		
		comum.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				
				JFrame frame = new JFrame("Snake Comum");
				frame.setContentPane(new InterfaceComum());
				frame.pack();
				frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				frame.setResizable(false);
				frame.pack();
				frame.setPreferredSize(new Dimension(InterfaceComum.WIDTH, InterfaceComum.HEIGHT));
				frame.setLocationRelativeTo(null);
				frame.setVisible(true);
				
				dispose();
				
			}
		});
		
		star.addMouseListener(new MouseAdapter(){
			@Override
			public void mouseClicked(MouseEvent e) {
				JFrame frame = new JFrame("Snake Star");
				frame.setContentPane(new InterfaceStar());
				frame.pack();
				frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				frame.setResizable(false);
				frame.pack();
				frame.setLocationRelativeTo(null);
				frame.setVisible(true);
				
				dispose();
			}
		});
		
		simples.addMouseListener(new MouseAdapter(){
			@Override
			public void mouseClicked(MouseEvent e) {
				JFrame frame = new JFrame("Atravessa parede");
				frame.setContentPane(new JogoSimples());
				frame.pack();
				frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				frame.setResizable(false);
				frame.pack();
				frame.setPreferredSize(new Dimension(JogoSimples.WIDTH, JogoSimples.HEIGHT));
				frame.setLocationRelativeTo(null);
				frame.setVisible(true);
				
				dispose();
			}
			});
		
		kitty.addMouseListener(new MouseAdapter(){
			@Override
			public void mouseClicked(MouseEvent e) {
				JFrame frame = new JFrame("Snake Kitty");
				frame.setContentPane(new InterfaceKitty());
				frame.pack();
				frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				frame.setResizable(false);
				frame.pack();
				frame.setPreferredSize(new Dimension(InterfaceKitty.WIDTH, InterfaceKitty.HEIGHT));
				frame.setLocationRelativeTo(null);
				frame.setVisible(true);
				
				dispose();
			}
			
		});
		}
		

	}




